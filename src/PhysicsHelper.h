#pragma once
#include <vector>
#include "ofGraphics.h"

class PhysicsHelper
{
public:
	static float PhysicsHelper::getForce(float m1, float m2, float g, float d)
	{
		return (m1 * m2 * g) / pow(d, 2);
	}
};
#pragma once
#include "math/ofVec2f.h"
#include "ofImage.h"

class GameObject
{
public:
	// Atributos finais
	ofColor tankColor;
	ofVec2f position;
	ofVec2f tankSize;
	ofVec2f turretSize;
	float tankRotation;
	ofColor turretColor;
	float turretRotation;

	// variaveis durante input do turno
	float old_tankRot = 0;
	float old_turretRot = 0;

	//Variáveis do turno
	int speed_turno = 0;
	int rotation_turno = 0;
	int rotation_turret = 0;

	GameObject(ofVec2f p_position, ofVec2f p_size, ofColor p_tank, ofColor p_turret, float p_rotation);
	GameObject();
	~GameObject();

	void setPosition(ofVec2f p_position);
	void translate(ofVec2f p_direction);
	void rotate(float p_rotation);
	void rotate_turret(float p_rotation);
	ofVec2f getCenterPosition();
	void draw();
};


#include "GameObject.h"
#include "ofApp.h"

GameObject::GameObject(ofVec2f p_position, ofVec2f p_tankSize, ofColor p_tank, ofColor p_turret, float p_rotation) {
	tankColor = p_tank;
	tankRotation = p_rotation;
	turretColor = p_turret;
	tankSize = p_tankSize;
	setPosition(p_position);
	turretRotation = 0.0f;
	turretSize = ofVec2f(tankSize.x, tankSize.y / 4);

	old_tankRot = tankRotation;
	old_turretRot = 0;
}

void GameObject::setPosition(ofVec2f p_position) {
	position = p_position - tankSize / 2;
}
void GameObject::translate(ofVec2f p_direction) {
	position = position + p_direction;
}

void GameObject::rotate(float p_rotation) {
	// Consiste as rota��es
	if (old_tankRot + p_rotation > old_tankRot + 30)
		p_rotation = 30;
	else if (old_tankRot + p_rotation < old_tankRot - 30)
		p_rotation = -30;

	tankRotation = p_rotation + old_tankRot;

	rotation_turno = p_rotation;
}

void GameObject::rotate_turret(float p_rotation) {
	// Consiste as rota��es
	if (old_turretRot + p_rotation > old_turretRot + 30)
		p_rotation = 30;
	else if (old_turretRot + p_rotation < old_turretRot - 30)
		p_rotation = -30;
	if (p_rotation + old_turretRot > 90)
		turretRotation = 90;
	else if (p_rotation + turretRotation < -90)
		turretRotation = -90;
	else
		turretRotation = p_rotation + old_turretRot;

	rotation_turret = p_rotation;
}

void GameObject::draw() {
	ofPushMatrix();
	ofTranslate(getCenterPosition());
	ofRotate(tankRotation, 0, 0, 1);
	ofSetColor(tankColor);
	ofRect(-tankSize.x / 2, -tankSize.y / 2, tankSize.x, tankSize.y);
	ofRotate(turretRotation, 0, 0, 1);
	ofTranslate(tankSize.x / 2, tankSize.y / 2 - turretSize.y / 2);
	ofSetColor(turretColor);
	ofRect(-tankSize.x / 2, -tankSize.y / 2, turretSize.x, turretSize.y);
	ofPopMatrix();
}

ofVec2f GameObject::getCenterPosition() {
	return ofVec2f(position.x + tankSize.x / 2, position.y + tankSize.y / 2);
}

GameObject::GameObject() {}
GameObject::~GameObject() {}

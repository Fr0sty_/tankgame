#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	tankGame = new TankGame();
	tankGame->init(ofVec2f(ofGetWindowWidth(), ofGetWindowHeight()));
	ofBackground(tankGame->backgroundColor);
}

//--------------------------------------------------------------
void ofApp::update() {
	tankGame->update(ofGetLastFrameTime());
}

//--------------------------------------------------------------
void ofApp::draw() {
	tankGame->draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {

	switch (tankGame->gameState)
	{
		case Player1:
			if (key == 'Q' || key == 'q')
				tankGame->tank1.rotation_turno -= 1;
			else if (key == 'E' || key == 'e')
				tankGame->tank1.rotation_turno += 1;

			if (key == 'A' || key == 'a')
				tankGame->tank1.rotation_turret -= 1;
			else if (key == 'D' || key == 'd')
				tankGame->tank1.rotation_turret += 1;

			if (key == 1 || key == '1')
				tankGame->tank1.speed_turno = 1;
			if (key == 2 || key == '2')
				tankGame->tank1.speed_turno = 2;
			if (key == 3 || key == '3')
				tankGame->tank1.speed_turno = 3;

			if (key == ' ')
			{
				tankGame->gameState = Player2;
				cout << "Turno Player2 --  Rotacao tank Q e E, Rotacao do canhao A e D, velocidade apertar 0, 1, 2, 3" << endl;
				cout << "Ao terminar as escolhas, aperte espa�o." << endl;
			}
			break;

		case Player2:
			if (key == 'Q' || key == 'q')
				tankGame->tank2.rotation_turno -= 1;
			else if (key == 'E' || key == 'e')
				tankGame->tank2.rotation_turno += 1;

			if (key == 'A' || key == 'a')
				tankGame->tank2.rotation_turret -= 1;
			else if (key == 'D' || key == 'd')
				tankGame->tank2.rotation_turret += 1;

			if (key == 1 || key == '1')
				tankGame->tank2.speed_turno = 1;
			if (key == 2 || key == '2')
				tankGame->tank2.speed_turno = 2;
			if (key == 3 || key == '3')
				tankGame->tank2.speed_turno = 3;

			if (key == ' ')
				tankGame->gameState = Playing;

			break;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}

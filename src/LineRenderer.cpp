#include "LineRenderer.h"

void LineRenderer::setPoints(vector<ofVec2f>* p_points)
{
	points = p_points;
}

void LineRenderer::draw()
{
	ofVec2f start;
	ofVec2f end;
	for (int i = 0; i < points->size(); i++)
	{
		start = ofVec2f((*points)[i].x, (*points)[i].y);

		if (i + 1 < points->size())
			end = ofVec2f((*points)[i + 1].x, (*points)[i + 1].y);
		else
			break;

		ofLine(start, end);
	}
}

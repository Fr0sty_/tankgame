#pragma once
#include <vector>
#include "ofImage.h"
#include "ofGraphics.h"
#include "math\ofVec2f.h"
#include "GameObject.h"
#include "VectorHelper.h"
#include "PhysicsHelper.h"

enum GameState {
	Player1 = 1,
	Player2 = 2,
	Playing = 3,
	GameOver = 4
};

class TankGame {
public:
	int gameState;
	//Consts
	const ofColor backgroundColor = ofColor(30, 30, 30);
	const ofColor drawColor = ofColor(255, 255, 255);

	//Game Objects
	GameObject tank1;
	GameObject tank2;

	//Methods
	void init(ofVec2f screenSize);
	void update(float deltaTime);
	void run_turn();
	void draw();
};

#include "TankGame.h"
#include "ofApp.h"

void TankGame::init(ofVec2f screenSize)
{
	gameState = Player1;
	// Initialize GameObjects
	tank1 = GameObject(ofVec2f(screenSize.x / 8, screenSize.y / 2), ofVec2f(30, 20), ofColor::darkRed, ofColor::orangeRed, 0.0f);
	tank2 = GameObject(ofVec2f(screenSize.x / 1.11f, screenSize.y / 2), ofVec2f(30, 20), ofColor::darkBlue, ofColor::deepSkyBlue, 180.0f);

	cout << "Turno Player1 --  Rotacao tank Q e E, Rotacao do canhao A e D, velocidade apertar 0, 1, 2, 3" << endl;
	cout << "Ao terminar as escolhas, aperte espa�o." << endl;
}

void TankGame::update(float deltaTime)
{
	switch (gameState)
	{
		case Player1:
			tank1.rotate(tank1.rotation_turno);
			tank1.rotate_turret(tank1.rotation_turret);
			
			break;

		case Player2:
			tank2.rotate(tank2.rotation_turno);
			tank2.rotate_turret(tank2.rotation_turret);

			break;

		case Playing:
			run_turn();

			break;

		case GameOver:
			// Printar quem venceu
			cout << "GameOver!" << endl;

			break;
	}
}

void TankGame::draw()
{
	tank1.draw();
	tank2.draw();
}

void TankGame::run_turn()
{
	//Atirar e movimentar bullet1
	//Atirar e movimentar bullet2

	//Translocar suavemente por frame
	tank1.translate(ofVec2f(10, 0) * tank1.speed_turno);
	tank2.translate(ofVec2f(10, 0) * tank2.speed_turno);

	tank1.old_tankRot = tank1.tankRotation;
	tank1.old_turretRot = tank1.turretRotation;
	tank1.rotation_turno = 0;
	tank1.rotation_turret = 0;
	tank1.speed_turno = 0;

	tank2.old_tankRot = tank2.tankRotation;
	tank2.old_turretRot = tank2.turretRotation;
	tank2.rotation_turno = 0;
	tank2.rotation_turret = 0;
	tank2.speed_turno = 0;

	gameState = Player1;
	cout << "Turno Player1 --  Rotacao tank Q e E, Rotacao do canhao A e D, velocidade apertar 0, 1, 2, 3" << endl;
	cout << "Ao terminar as escolhas, aperte espa�o." << endl;
}